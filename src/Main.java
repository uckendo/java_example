import java.lang.reflect.Constructor;

public class Main{
    public static void main(String[] args)throws Exception{
        Constructor cons1 = Integer.class.getConstructor(int.class);
        Integer n1 = (Integer) cons1.newInstance(123);
        System.out.println(n1.getClass().toString());
        System.out.println(n1);

        Constructor cons2 = Integer.class.getConstructor(String.class);
        Integer n2 = (Integer) cons2.newInstance("345");
        System.out.println(n2.getClass().toString());
        System.out.println(n2);

        Constructor cons3 = Double.class.getConstructor(double.class);
        Double n3 = (Double) cons3.newInstance(3.1);
        System.out.println(n3.getClass().toString());
        System.out.println(n3);
    }
}